from flask import Flask, jsonify, request
import json
import logging

app = Flask(__name__)

gunicorn_error_logger = logging.getLogger('gunicorn.error')
app.logger.handlers.extend(gunicorn_error_logger.handlers)
app.logger.setLevel(logging.INFO)


info = [
    { 'api': 'user', 'version': '1.0'}
]

@app.route('/info')
def get_info():
    return jsonify(info)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True, threaded=True)
